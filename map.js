var Promise = require("bluebird");

Promise.try(function(){
	return getItemsSomehow()
}).map(function(topLevelItem){
	return Promise.try(function(){
		return doAsyncThingWith(topLevelItem);
	}).map(function(childItem){
		return doOtherAsyncThingWithEach(childItem);
	});
}).then(function(finalResult){
	/* finalResult will contain an array of arrays, all of it asynchronously resolved. */
})